FROM python:2-onbuild

COPY entrypoint.sh /entrypoint.sh

RUN \
useradd -ms /usr/bin/nologin www &&\
python -c "from application import init_db; init_db()"


ENV VIRTUAL_HOST revisions.web.hackfortress.net

EXPOSE 80

CMD ["/entrypoint.sh"]
