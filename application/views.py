#!/usr/bin/env python

import hashlib
from application import app, query_db
from flask import render_template, flash, redirect
from .forms import LoginForm


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Client Portal')


def authenticate(username, password):
    user = query_db("select * from users where username = ?",
                    [username], one=True)
    if user is None or user['password'] != password:
        return False
    else:
        return True

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested')
        # Check login information
        username = form.username.data
        password = form.password.data
        bypass = False
        if username == 'admin' and password == app.config['ADMIN_BYPASS']:
            bypass = True

        if authenticate(username, password) or bypass:
            with open(app.config['SOLUTION_FILE']) as f:
                solution = f.read()
            return render_template('solution.html', solution=solution)
        else:
            return render_template('login.html', title='Login', form=form, failed=True)


    # else
    return render_template('login.html', title='Login', form=form, failed=False)
