CREATE TABLE users(
    id INT PRIMARY KEY NOT NULL,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    admin NUMERIC DEFAULT 0
);
