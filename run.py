#!/usr/bin/env python

from application import app

LISTEN_HOST = '0.0.0.0'
LISTEN_PORT = 8080

app.run(debug=True, host=LISTEN_HOST, port=LISTEN_PORT)
