#!/bin/bash

echo ${SOLUTION} > /var/tmp/solution.txt

/usr/local/bin/uwsgi --shared-socket :80 --http =0 --processes 1 --threads 2 -w application:app --uid www --gid www --enable-threads
